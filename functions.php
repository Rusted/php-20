<?php declare (strict_types = 1);

const HOST = 'localhost';
const DB_USER = 'root';
const DB_PASSWORD = '';
const DATABASE = 'baltic_talents';
const NPD = 149;
const INCOME_TAX_PERCENT = 0.15;
const HEALTH_INSURANCE_PERCENT = 0.06;
const SOCIAL_INSURANCE_PERCENT = 0.06;
const SODRA_PERCENT = 0.3098;
const WARANTY_FUND_PERCENT = 0.002;

function getConnection(): PDO
{
    $pdo = new PDO('mysql:host=' . HOST . ';dbname=' . DATABASE . ';charset=utf8mb4', DB_USER, DB_PASSWORD);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);

    return $pdo;
}

function createEmployee(PDO $pdo, string $name, string $surname, string $gender, string $birthday, string $education, int $salary, int $idarbinimoTipas, int $pareigosId): bool
{
    $sql = 'INSERT into darbuotojai(name, surname, gender, birthday, education, salary, idarbinimo_tipas, pareigos_id)
     VALUES (:name, :surname, :gender, :birthday, :education, :salary,  :idarbinimo_tipas, :pareigos_id)';

    $query = $pdo->prepare($sql);
    return $query->execute([
        'name' => $name,
        'surname' => $surname,
        'gender' => $gender,
        'birthday' => $birthday,
        'education' => $education,
        'salary' => $salary,
        'idarbinimo_tipas' => $idarbinimoTipas,
        'pareigos_id' => $pareigosId,
    ]);
}

function updateEmployee(PDO $pdo, int $id, string $name, string $surname, string $gender, string $birthday, string $education, int $salary, int $idarbinimoTipas, int $pareigosId): bool
{
    $sql = 'UPDATE darbuotojai
    SET name=:name,
        surname=:surname,
        gender=:gender,
        birthday=:birthday,
        education=:education,
        salary=:salary,
        idarbinimo_tipas=:idarbinimo_tipas,
        pareigos_id=:pareigos_id
    WHERE id=:id';

    $query = $pdo->prepare($sql);

    return $query->execute([
        'id' => $id,
        'name' => $name,
        'surname' => $surname,
        'gender' => $gender,
        'birthday' => $birthday,
        'education' => $education,
        'salary' => $salary,
        'idarbinimo_tipas' => $idarbinimoTipas,
        'pareigos_id' => $pareigosId,
    ]);
}

function getAllPositions(PDO $pdo): array
{
    $stmt = $pdo->prepare('SELECT id, name, base_salary FROM pareigos');
    $stmt->execute();

    return $stmt->fetchAll();
}

function getPosition(PDO $pdo, int $id)
{
    $stmt = $pdo->prepare('SELECT id, name, base_salary FROM pareigos WHERE id=:id');
    $stmt->execute(['id' => $id]);

    return $stmt->fetch();
}

function createPosition(PDO $pdo, string $name, string $salary): bool
{
    $sql = 'INSERT into pareigos(name, base_salary)
     VALUES (:name, :base_salary)';

    $query = $pdo->prepare($sql);
    return $query->execute([
        'name' => $name,
        'base_salary' => $salary,
    ]);
}

function updatePosition(PDO $pdo, int $id, string $name, int $baseSalary)
{
    $sql = 'UPDATE pareigos SET name=:name, base_salary=:base_salary WHERE id=:id';
    $stmt = $pdo->prepare($sql);
    $stmt->execute(['id' => $id, 'name' => $name, 'base_salary' => $baseSalary]);
}

function getEmployee(PDO $pdo, int $employeeId): array
{
    $stmt = $pdo->prepare('SELECT id, name, surname, education, salary, phone, birthday, pareigos_id, idarbinimo_tipas FROM darbuotojai WHERE id =:id');
    $stmt->execute(['id' => $employeeId]);
    return $stmt->fetch();
}

function getAllEmployees(PDO $pdo): array
{
    $stmt = $pdo->prepare('SELECT id, name, surname, education, salary, phone FROM darbuotojai');
    $stmt->execute();

    return $stmt->fetchAll();
}

function getEmployeeCounts(PDO $pdo): array
{
    $stmt = $pdo->prepare('SELECT pareigos_id, count(*) employee_count FROM darbuotojai GROUP BY pareigos_id');
    $stmt->execute();

    return $stmt->fetchAll(PDO::FETCH_KEY_PAIR);
}

function getTotalEmployeeCount(PDO $pdo): int
{
    $stmt = $pdo->prepare('SELECT COUNT(*) employee_count FROM darbuotojai');
    $stmt->execute();
    $result = $stmt->fetch();

    return (int) $result['employee_count'];
}

function getGenderStatistics(PDO $pdo)
{
    $stmt = $pdo->prepare('SELECT gender, COUNT(*) employee_count FROM darbuotojai GROUP BY gender');
    $stmt->execute();

    return $stmt->fetchAll();
}

function getEducationSalaryStatistics(PDO $pdo): array
{
    $stmt = $pdo->prepare('SELECT education, COUNT(*) employee_count, AVG(salary) average_salary FROM darbuotojai GROUP BY education');
    $stmt->execute();

    return $stmt->fetchAll();
}

function deletePosition(PDO $pdo, int $id): bool
{
    $stmt = $pdo->prepare('DELETE FROM pareigos WHERE id=:id');
    return $stmt->execute(['id' => $id]);
}

function deleteEmployee(PDO $pdo, int $id): bool
{
    $stmt = $pdo->prepare('DELETE FROM darbuotojai WHERE id=:id');
    return $stmt->execute(['id' => $id]);
}

function getSalaryData(float $salary): array
{
    $incomeTax = ($salary - NPD) * INCOME_TAX_PERCENT;
    $healthSecurityTax = $salary * HEALTH_INSURANCE_PERCENT;
    $socialSecurityTax = $salary * SOCIAL_INSURANCE_PERCENT;
    $netSalary = $salary - $incomeTax - $healthSecurityTax - $socialSecurityTax;

    return [
        'income_tax' => $incomeTax,
        'health_security_tax' => $healthSecurityTax,
        'social_security_tax' => $socialSecurityTax,
        'net_salary' => $netSalary,
    ];
}

function getExpenses(float $salary): array
{
    return [
        'sodra' => $salary * SODRA_PERCENT,
        'warranty_fund' => $salary * WARANTY_FUND_PERCENT,
        'salary' => $salary,
        'total' => $salary * SODRA_PERCENT + $salary * WARANTY_FUND_PERCENT + $salary,
    ];
}

function getEmployeesByPosition(PDO $pdo, int $positionId): array
{
    $stmt = $pdo->prepare('SELECT id, name, surname, education, salary, phone FROM darbuotojai WHERE pareigos_id = :position_id ');
    $stmt->execute(['position_id' => $positionId]);

    return $stmt->fetchAll();
}
