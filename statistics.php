<?php
require_once 'functions.php';
$pdo = getConnection();
$educationSalaryStatistics = getEducationSalaryStatistics($pdo);
$employeeCount = getTotalEmployeeCount($pdo);
$genderStatistics = getGenderStatistics($pdo);
?>
<html>
<body>
<a href="index.php">Atgal</a>
    <h1>Darbuotojų išsilavinimo statistika</h1>
    <table border="3">
        <tr>
            <th>Išsilavinimas</th>
            <th>Darbuotojų kiekis</th>
            <th>Vidutinis darbo užmokestis</th>
        <?php foreach ($educationSalaryStatistics as $educationSalaryStatisticsItem) {?>
        <tr>
            <td><?php echo $educationSalaryStatisticsItem['education']; ?></td>
            <td><?php echo $educationSalaryStatisticsItem['employee_count']; ?></td>
            <td><?php echo round($educationSalaryStatisticsItem['average_salary'], 2); ?></td>
        </tr>
        <?php }?>
        </tr>
    </table>

    <h1>Darbuotojų lyties statistika</h1>
    <table border="3">
        <tr>
            <th>Lytics</th>
            <th>Darbuotojų kiekis</th>
        <?php foreach ($genderStatistics as $genderStatisticsItem) {?>
        <tr>
            <td><?php echo $genderStatisticsItem['gender']; ?></td>
            <td><?php echo round(100 * $genderStatisticsItem['employee_count'] / $employeeCount, 2); ?>%</td>
        </tr>
        <?php }?>
        </tr>
    </table>
</body>

</html>
